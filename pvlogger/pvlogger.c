#include <stdio.h>
#include <stdlib.h>
#include <stdint.h>
#include <unistd.h>
#include <fcntl.h>
#include <string.h>
#include <errno.h>

#include <pthread.h>

#include <sys/types.h>
#include <sys/time.h>
#include <sys/socket.h>
#include <sys/un.h>

typedef	struct {
	char data[2048];
} log_entry_t;

typedef struct {
	void *buf_start;
	void *buf_end;
	log_entry_t *head;
	log_entry_t *tail;
	log_entry_t *last;
	uint32_t buf_size;
	uint32_t size;
	uint32_t count;
} log_buf_t;

struct data {
	log_buf_t *lb;;
	char *src;
};

pthread_mutex_t lock;

int open_socket(char *path)
{
	int fd, ret;
	struct sockaddr_un addr;
	long arg;

	fd = socket(AF_UNIX, SOCK_STREAM, 0);
	if (fd < 0)
		return 1;

	memset(&addr, 0, sizeof(addr));
	addr.sun_family = AF_UNIX;

	strcpy(addr.sun_path, path);

	arg = fcntl(fd, F_GETFL, NULL);
	arg |= O_NONBLOCK;
	fcntl(fd, F_SETFL, arg);

	ret = connect(fd, (struct sockaddr*)&addr, sizeof(addr));
	while (ret < 0) {
		usleep(10000);
		ret = connect(fd, (struct sockaddr*)&addr, sizeof(addr));
	}

	if (ret)
		return 1;

	arg = fcntl(fd, F_GETFL, NULL);
	arg &= (~O_NONBLOCK);
	fcntl(fd, F_SETFL, arg);

	return fd;
}

static void log_add(log_buf_t *lb, char *buf)
{
	pthread_mutex_lock(&lock);

	log_entry_t e;

	// set data
	strcpy(e.data, buf);

	lb->last = lb->head;
	memcpy(lb->head, &e, sizeof(log_entry_t));
	lb->head += 1;
	if (lb->head == lb->buf_end) {
		lb->head = lb->buf_start;
	}

	if (lb->count < lb->size)
		lb->count++;

	pthread_mutex_unlock(&lock);
}

static void *looper(void *arg)
{
	struct data *d = (struct data *) arg;

	log_buf_t *lb = d->lb;

	char buf[BUFSIZ];
	fpos_t pos;
	FILE *fp = stdin;

	if (strcmp("stdin", d->src)) {
		printf("%s():%d in='%s'\n", __func__, __LINE__, d->src);
		fp = fopen(d->src, "r");
	}

	if (!fp)
		return NULL;

	fseek(fp, 0, SEEK_END);
	fgetpos(fp, &pos);
	setvbuf(fp, NULL, _IOLBF, BUFSIZ);

	for (;;) {
		int sleep=0;
		if (!fgets(buf, BUFSIZ, fp)) {
			sleep=1;
		}
		if (feof(fp)) { 
			fsetpos(fp, &pos);
			clearerr(fp);
			sleep=1;
		}
		if (sleep) {
			usleep(500000);
			continue;
		}
		fgetpos(fp, &pos);
		log_add(lb, buf);
		fflush(stdout);
	}

	printf("Thread exit\n");
	return NULL;
}

static void log_get_next(log_buf_t *lb, log_entry_t **e)
{
	log_entry_t *next;

	next = (*e)+1;
	if (next == lb->buf_end)
		next = lb->buf_start;

	if (next == lb->tail)
		next = NULL;

	if (next->data == NULL)
		next = NULL;
	else if (!strcmp(next->data, ""))
		next = NULL;

	*e = next;
	return;
}

static void log_reset(log_buf_t *lb)
{
	if (!lb)
		return;

	memset(lb->buf_start, 0, lb->buf_size);
	lb->tail = lb->buf_start;
	lb->head = lb->buf_start;
	lb->count = 0;
}

int main(int argc, char *argv[])
{
	int fd;
	pthread_t tid;
	pthread_attr_t attr;
	log_buf_t *lb;
	struct data *d;

	pthread_mutex_init(&lock, NULL);

	lb = calloc(1, sizeof(log_buf_t));
	if (!lb)
		return 0;

	lb->size = 256;
	lb->buf_size = sizeof(log_entry_t) * lb->size;
	lb->buf_start = calloc(1, lb->buf_size);
	lb->buf_end = (char *) lb->buf_start + lb->buf_size;
	lb->head = (log_entry_t*) lb->buf_start;
	lb->tail = lb->buf_start;

	d = calloc(1, sizeof(struct data));
	d->lb = lb;

	if (argc != 2)
		d->src = strdup("/var/log/messages");
	else if (strcmp(argv[1], "stdin"))
		d->src = strdup(argv[1]);
	else
		d->src = strdup("stdin");

	pthread_create(&tid, NULL, &looper, d);

	log_entry_t *e = lb->tail;
	for (;;) {
		pthread_mutex_lock(&lock);
		if (strcmp(e->data, "")) {
			char cmd[4096];
			sprintf(cmd, "\x02" "%s", e->data);
			fd = open_socket("/pantavisor/pv-ctrl");
			write(fd, cmd, strlen(cmd));
			close(fd);
		}
		log_get_next(lb, &e);
		if (!e) {
			log_reset(lb);
			e = lb->tail;
		    pthread_mutex_unlock(&lock);
		    usleep(100000);
		} else {
		    pthread_mutex_unlock(&lock);
		}
	}

	return 0;
}
